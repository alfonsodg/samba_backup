#!/bin/bash

echo "Instalando SAMBA"
yum -y install cups-libs samba samba-common samba-winbind

echo "Configurando SAMBA"
mv /etc/samba/smb.conf /etc/samba/smb.conf.orig
cp -f smb.conf /etc/samba/

echo "Levantando servicios"
chkconfig --levels 235 smb on
service smb start
service nmb start

echo "Creacion de grupos"
groupadd estaciones
groupadd usuarios

echo "Usuario administrador"
smbpasswd -a root
smbpasswd -e root

echo "Usuarios varios"
#useradd -d /dev/null -g estaciones -s /sbin/nologin MAQUINA$
#useradd -g usuarios USUARIO
#smbpasswd -m -a MAQUINA
#smbpasswd -a USUARIO

echo "Creando directorios netlogon y profiles"
mkdir /var/lib/samba/netlogon
mkdir /var/lib/samba/profiles

echo "Implementando scripts de inicio"
cp -f logon.cmd /var/lib/samba/netlogon/
cp -f DSynchronize.exe /var/lib/samba/netlogon/
cp -f DSynchronize.ini /var/lib/samba/netlogon/

cp -f default_user.tar.gz /var/lib/samba/profiles/
cd /var/lib/samba/profiles
tar xvfz /var/lib/samba/profiles/default_user.tar.gz

echo "Privilegios samba"
net rpc rights grant root SeMachineAccountPrivilege
setenforce Permissive
setsebool -P samba_domain_controller on 
setsebool -P samba_enable_home_dirs on
setsebool -P samba_export_all_rw on

echo "Reiniciando servicios"
service smb restart
service nmb restart
